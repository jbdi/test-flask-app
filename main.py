# coding: utf-8
from __future__ import unicode_literals
from datetime import datetime
import json

from flask import (Flask, g, jsonify, request,
                   Response, render_template, url_for)
from flask_cache import Cache
import psycopg2
import psycopg2.extras

from utils import db_exec


app = Flask(__name__)
app.config.from_object(__name__)
app.config.update(dict(
    SECRET_KEY='',
    DATABASE='selectel',
    USERNAME='selectel',
    PASSWORD=''
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
app.config['JSON_AS_ASCII'] = False
cache = Cache(app, config={'CACHE_TYPE': 'memcached'})


class Statuses(object):
    OPEN = 0
    ANSWERED = 1
    ANSWER_PENDING = 2
    CLOSED = 3

    rules = {
            OPEN:           [ANSWERED, CLOSED],
            ANSWERED:       [ANSWER_PENDING, CLOSED],

            # не описано на что можно, поэтому пусть будет так
            ANSWER_PENDING: [ANSWERED, CLOSED],
            CLOSED:         [],
        }
    default_status = OPEN

    def check(self, ticket_status, new_status):
        return new_status in self.rules.get(ticket_status)


def connect_db():
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)
    conn = psycopg2.connect(dbname=app.config['DATABASE'],
                            user=app.config['USERNAME'])
    return conn


def get_db():
    if not hasattr(g, 'postgres_db'):
        g.postgres_db = connect_db()
    return g.postgres_db


def get_cursor():
    db = get_db()
    cur = db.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    return cur


@app.cli.command('create_db_structure')
def create_db_structure():
    ticket_sql = """
        CREATE TABLE ticket (
            id serial NOT NULL PRIMARY KEY,
            date_created timestamp with time zone NOT NULL,
            date_updated timestamp with time zone DEFAULT now(),
            subject character varying(512) NOT NULL,
            text text,
            email character varying(128) NOT NULL,
            status smallint DEFAULT %s
        );"""

    comment_sql = """
        CREATE TABLE comment (
            id serial NOT NULL PRIMARY KEY,
            ticket_id bigint NOT NULL REFERENCES ticket(id) ON DELETE CASCADE,
            date_created timestamp with time zone NOT NULL,
            email character varying(255) NOT NULL,
            text text NOT NULL
        );"""
    db = get_db()
    cursor = get_cursor()

    try:
        cursor.execute(ticket_sql, (Statuses.default_status,)) #NOTE: check it!
        cursor.execute(comment_sql)
        db.commit()
    except psycopg2.Error as e:
        raise e


@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'postgres_db'):
        g.postgres_db.close()


def get_ticket(cursor, ticket_id):
    cursor.execute("SELECT * FROM ticket WHERE id=%s", [ticket_id])
    ticket = cursor.fetchone()
    return ticket


@app.route('/')
def index():
    # NOTE: странная ошибка при запуске приложения через uwsgi, хоть приложение
    #       и использует unicode_literals, но uwsgi это игнорирует
    urls = [
        (
            u'Добавление тикета',
            url_for('ticket_add', subject='Заголовок тикета', text='Описание тикета', email='test@example.com'),
        ),
        (
            u'Ссылка на тикет id=1',
            url_for('ticket', ticket_id=1),
        ),
        (
            u'Добавление тикета с ошибкой',
            url_for('ticket_add', text='Описание тикета', email='test@example.com'),
        ),
        (
            u'Смена статуса у тикета с id=1, OPEN на ANSWERED',
            url_for('ticket_new_status', ticket_id=1, new_status=Statuses.ANSWERED),
        ),
        (
            u'Смена статуса у тикета с id=1, ANSWERED на ANSWER_PENDING',
            url_for('ticket_new_status', ticket_id=1, new_status=Statuses.ANSWER_PENDING),
        ),
        (
            u'Смена статуса у тикета с id=1, ANSWER_PENDING на CLOSED',
            url_for('ticket_new_status', ticket_id=1, new_status=Statuses.CLOSED),
        ),
        (
            u'Смена статуса у тикета с id=1, CLOSED на OPEN',
            url_for('ticket_new_status', ticket_id=1, new_status=Statuses.OPEN),
        ),
        (
            u'Добавление комментария для тикета id=1',
            url_for('ticket_add_comment', ticket_id=1, text='Текст комментария для тикета', email='mail@example.com'),
        ),
        (
            u'Добавление комментария с ошибкой для тикета id=1',
            url_for('ticket_add_comment', ticket_id=1, email='mail@example.com'),
        ),
    ]
    return render_template('index.html', urls=urls)


@app.route('/ticket/add/', methods=['GET']) # GET for example, POST needed
def ticket_add():
    db = get_db()
    cursor = get_cursor()

    subject = request.args.get('subject')
    text = request.args.get('text')
    email = request.args.get('email')
    if not all([subject, text, email]):
        response_context = {'success': False,
                            'error': 'one of required fields is empty'}
        return jsonify(response_context)

    ticket_query = """
        INSERT INTO ticket (subject, text, email, date_created, status)
        VALUES(%s, %s, %s, %s, %s);"""
    ticket_values = (subject, text, email, datetime.utcnow(),
                     Statuses.default_status,)
    response = db_exec(cursor,
                       query=ticket_query,
                       query_args=ticket_values,
                       fetch=None,
                       db_connect=db,
                       commit=True)
    return jsonify(response)


@app.route('/ticket/<int:ticket_id>/')
@cache.cached(timeout=60*5, key_prefix='ticket/%s')
def ticket(ticket_id):
    cursor = get_cursor()
    ticket = db_exec(cursor,
                     query="SELECT * FROM ticket WHERE id=%s",
                     query_args=(ticket_id,),
                     fetch='fetchone',
                     db_connect=None,
                     commit=False)
    if ticket and ticket.get('item'):
        # json.dumps can't handle datetime with timezone
        ticket['item']['date_updated'] = unicode(ticket['item']['date_updated'])
        ticket['item']['date_created'] = unicode(ticket['item']['date_created'])
        response_json = json.dumps(ticket, ensure_ascii=False)
    else:
        response_json = json.dumps({'success': False}, ensure_ascii=False)
    return Response(response_json,
                    content_type="application/json; charset=utf-8")


@app.route('/ticket/<int:ticket_id>/new_status/<int:new_status>', methods=['GET']) # GET for example, POST needed)
def ticket_new_status(ticket_id, new_status):
    db = get_db()
    cursor = get_cursor()
    ticket = get_ticket(cursor, ticket_id)
    ticket_status = ticket['status']
    valid_new_status = Statuses().check(ticket_status, new_status)
    if not valid_new_status:
        return jsonify({
                'success': False,
                'error': 'Not valid new status for ticket "{}"'\
                         .format(ticket_id)
                })
    response = db_exec(cursor,
                       query="UPDATE ticket SET status=%s WHERE id=%s",
                       query_args=[new_status, ticket_id],
                       fetch=None,
                       db_connect=db,
                       commit=True)
    return jsonify(response)


@app.route('/ticket/<int:ticket_id>/comment/add/', methods=['GET']) # GET for example, POST needed
def ticket_add_comment(ticket_id):
    db = get_db()
    cursor = get_cursor()
    ticket = get_ticket(cursor, ticket_id)
    if not ticket:
        return jsonify({
                'success': False,
                'error': 'Ticket with id="{} not found"'.format(ticket_id)
                })

    if ticket['status'] == Statuses.CLOSED:
        return jsonify({
                'success': False,
                'error': 'Can not leave a comment, ticket closed.'
                })

    text = request.args.get('text')
    email = request.args.get('email')
    if not all([text, email]):
        response_context = {'success': False,
                            'error': 'one of required fields is empty'}
        return jsonify(response_context)

    comment_query = """
        INSERT INTO comment (text, email, date_created, ticket_id)
        VALUES(%s, %s, %s, %s);"""
    comment_values = (text, email, datetime.utcnow(), ticket['id'])
    response = db_exec(cursor,
                       query=comment_query,
                       query_args=comment_values,
                       fetch=None,
                       db_connect=db,
                       commit=True)
    return jsonify(response)

