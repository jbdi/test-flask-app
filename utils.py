# coding: utf-8
from __future__ import unicode_literals

import psycopg2


class FetchError(ValueError):
    pass

def db_exec(cursor, query, query_args, fetch, db_connect=None, commit=False):
    if fetch and not fetch in ('fetchone', 'fetchall',):
        raise FetchError('fetch must be equal either "fetchall" or "fetchone"')

    response = {}
    try:
        cursor.execute(query, query_args)

        if fetch == 'fetchone':
            item = cursor.fetchone()
            response = {'success': True, 'item': item,}

        elif fetch == 'fetchall':
            items = cursor.fetchall()
            response = {'success': True, 'items': items,}

        if commit:
            db_connect.commit()
            response = {'success': True}

    except psycopg2.Error as e:
        response = {'success': False, 'error': e.message,}
    return response

