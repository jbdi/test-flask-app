Чтобы запустить, нужно:

* создать бд selectel и пользователя selectel
* создать окружение, например, mkvirtualenv selectel
* в файле uwsgi.ini исправить пути до виртуального окружения в переменных home и pythonpath

запустить команды, для создания таблиц
> export FLASK_APP=main.py
> flask create_db_structure

запустить сервер
> flask run

либо
> uwsgi --ini uwsgi.ini

Список ссылок для тестирования доступен на главной странице